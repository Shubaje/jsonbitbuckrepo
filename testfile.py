import json

jsonstr='''
{
  "firstName": "John",
  "lastName" : "doe",
  "age"      : 26,
  "address"  : {
    "streetAddress": "naist street",
    "city"         : "Nara",
    "postalCode"   : "630-0192"
  },
  "phoneNumbers": [
    {
      "type"  : "iPhone",
      "number": "0123-4567-8888"
    },
    {
      "type"  : "home",
      "number": "0123-4567-8910"
    }
  ]
}

'''

strDict=json.loads(jsonstr)
print(strDict)
print(type(strDict['phoneNumbers']))
for  i in strDict['phoneNumbers']:
    print(i)
    print(i['number'])

for  i in strDict['phoneNumbers']:
    print(i)
    del i['number']

newStr=json.dumps(strDict, indent=2)
print(newStr)

newStr=json.dumps(strDict, sort_keys=True)
print(newStr)