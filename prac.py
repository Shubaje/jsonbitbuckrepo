import json

strJson='''
{
  "firstName": "John",
  "lastName" : "doe",
  "age"      : 26,
  "address"  : {
    "streetAddress": "naist street",
    "city"         : "Nara",
    "postalCode"   : "630-0192"
  },
  "phoneNumbers": [
    {
      "type"  : "iPhone",
      "number": "0123-4567-8888"
    },
    {
      "type"  : "home",
      "number": "0123-4567-8910"
    }
  ]
}

'''

data=json.loads(strJson)
print(data)
print(type(data))

print(type(data['phoneNumbers']))
for i in data['phoneNumbers']:
    print(i)


for i in data['phoneNumbers']:
    del i['type']



strNew=json.dumps(data,sort_keys=True)
print(strNew)

strNew=json.dumps(data,sort_keys=True,indent=2)
print(strNew)